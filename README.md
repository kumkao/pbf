Purpose
==================================
PBF is the abbreviation of PHP Basic Framework.
The minimal framework developed with the basic of PHP.
The structure of this project is not the MVC. We just
want the newbie to understand the basic of web development
(with good enough structure).

Setup Instructions
==================================
1. change the database setting in settings/database.php
2. change the site url in settings/application.php

Logging
==================================
The minimal log is stored in sql_logger directory (by default).
The log will show the request URL and all executed queries.

CSS/JS/IMAGE/ETC
====================================
All the style sheet, javascript, images and other frontend library
will be store in assets directory.

Templates & Views
=====================================
All views or templates will be store in views directory (by default).
the view can be called by the 'render()' function (core/helper.php).
We can pass the list of variables to the view for rendering with
the associate key array format.

Database Usage
======================================
The 'mysqli' variable will be preloaded (in startup.php). We can
use mysqli after require the 'startup.php'.

demo user
======================================
username: kumkao
password: 1234
