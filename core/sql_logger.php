<?php
	function sql_logger($message) {
		$path = SQL_LOGGER_DIRECTORY .'/'. SQL_LOGGER_NAME;
		$logger = fopen($path,'a+');
		fwrite($logger,$message."\n");
		fclose($logger);
		$_SESSION['sql_logger'] .= $message;
	}

	function flush_log() {
		$_SESSION['sql_logger'] = '';
	}
