<?php
	function redirect($url) {
		header('Location: '.SITE_URL."/{$url}");
	}

	function render($view,$passing_data=NULL) {
		if( is_array($passing_data) ) {
			extract($passing_data);
		}
		ob_start();
		require(VIEW_DIRECTORY.'/'.$view);
		$output = ob_get_contents();
		ob_end_clean();
		echo $output;
	}
	function make_password($raw_password) {
		return md5($raw_password);
	}
