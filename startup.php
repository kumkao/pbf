<?php
	// setting loader
	require('settings/application.php');
	require('settings/database.php');
	// dependencies loader
	require('core/sql_logger.php');
	require('core/MySQLiExtend.php');
	require('core/database.php');
	require('core/helper.php');


	// startup tasks
	session_start();
	flush_log();
	sql_logger("\n\nhttp://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
